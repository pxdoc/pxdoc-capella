var BrushBase = require('brush-base');
var regexLib = require('syntaxhighlighter-regex').commonRegExp;

function Brush() {
  var keywords = ' it assert boolean break byte case catch char class const ' +
    'continue default do double else enum extends ' +
    'false final finally float for goto if implements import ' +
    'instanceof int interface long native new null ' +
    'package private protected public return override ' +
    'short static strictfp super switch synchronized this throw throws true ' +
    'transient try void volatile while log template function var val root apply separator';
  var pxdocModifierKeywords = 'bold italic underline strike color';
  var pxdocKeywords = 'docObjects subscript superscript language samePage bold italic underline strike color pxDocGenerator pxDocModule document table row cell hyperlink bookmark HeadingN toc subDocument image field font newLine newPage section §';
  var pxdocProperties = ' class: align: attachedTemplate: backgroundColor: bookmark: bullet numbering cantSplit comment: color: description: fileName: footerHeight: fromTemplate: header headerHeight: height: href: java: keepWithNext landscape left: level: merge: name: parameters: path: properties: shadingColor: shadingPattern: shift: size: strike styleBindings: styles: stylesheet: title: titleStyle: type: variables: width: with: displayString:';
  
  this.regexList = [
    {
      regex: regexLib.singleLineCComments,
      css: 'comments'
    },
    {
      regex: /\/\*([^\*][\s\S]*?)?\*\//gm,
      css: 'comments'
    },
    {
      regex: /\/\*(?!\*\/)\*[\s\S]*?\*\//gm,
      css: 'preprocessor'
    },
    {
      regex: regexLib.doubleQuotedString,
      css: 'string'
    },
    {
      regex: regexLib.singleQuotedString,
      css: 'string'
    },
    {
      regex: /\b([\d]+(\.[\d]+)?f?|[\d]+l?|0x[a-f0-9]+)\b/gi,
      css: 'value'
    },
    {
      regex: /(?!\@interface\b)\@[\$\w]+\b/g,
      css: 'color1'
    },
    {
      regex: /\@interface\b/g,
      css: 'color2'
    },
    {
      regex: new RegExp(this.getKeywords(keywords), 'gm'),
      css: 'keyword'
    },
    {
        regex: new RegExp(this.getKeywords(pxdocKeywords), 'gm'),
        css: 'pxdocKeyword'
    },
    {
        regex: new RegExp(this.getKeywords(pxdocProperties).slice(0,-2), 'gm'),
        css: 'pxdocProperty'
    },
    {
        regex: /(?:^|\W)#(\w+)(?!\w)/g,
        css: 'pxdocStyle'
    },
    {
        regex: /(?:§)/g,
        css: 'pxdocKeyword'
    }
  ];

  this.forHtmlScript({
    left: /(&lt;|<)%[@!=]?/g,
    right: /%(&gt;|>)/g
  });
};

Brush.prototype = new BrushBase();
Brush.aliases = ['pxdoc'];
module.exports = Brush;