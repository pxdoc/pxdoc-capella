package org.pragmaticmodeling.pxdoc.runtime.ui.capella;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.emf.ecore.EObject;
import org.polarsys.capella.common.helpers.TransactionHelper;

import com.google.inject.Inject;
import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.guice.IGuiceAware;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public abstract class CommandHandler extends AbstractHandler implements IGuiceAware {

	@Inject
	IStylesheetsRegistry stylesheetsRegistry;

	@Inject
	IResourceProvider provider;
	
	@Inject
	AbstractPxUiPlugin uiActivator;
	
	@Inject 
	IModelProvider modelProvider;

	private Injector injector;

	protected Collection<?> getInitialSelection(Object evaluationContext) {
		IEvaluationContext context = (IEvaluationContext) evaluationContext;
		return getSemanticObjects((Collection<?>) context.getDefaultVariable());
	}

	private Collection<EObject> getSemanticObjects(Collection<?> selection) {
		return ((DefaultCapellaModelProvider)modelProvider).getSemanticObjects(selection);
	}

	protected abstract IPxDocCommand createCommand(Collection<?> selection);

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		return execute(getSelection(event));
	}

	public Object execute(Collection<?> selection) throws ExecutionException {
		IPxDocCommand cmd = createCommand(selection);
		cmd.setStylesheetRegistry(stylesheetsRegistry);
		cmd.setResourceProvider(provider);
		cmd.setUiActivator(uiActivator);
		cmd.initialize(injector);
		Collection<? extends EObject> semanticElements = getSemanticObjects(selection);
		TransactionHelper.getExecutionManager(semanticElements).execute(cmd);
		return null;
	}

	protected Collection<?> getSelection(ExecutionEvent event) {
		IEvaluationContext context = (IEvaluationContext) event.getApplicationContext();
		Object selection = context.getDefaultVariable();
		if (selection instanceof Collection) {
			return (Collection<?>) selection;
		}
		return Collections.emptyList();
	}
	
	@Override
	public void initialize(Injector injector) {
		this.injector = injector;
	}
}