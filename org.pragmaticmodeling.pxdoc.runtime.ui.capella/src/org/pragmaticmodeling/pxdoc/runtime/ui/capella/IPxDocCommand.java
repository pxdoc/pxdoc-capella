package org.pragmaticmodeling.pxdoc.runtime.ui.capella;

import org.polarsys.capella.common.ef.command.ICommand;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.guice.IGuiceAware;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public interface IPxDocCommand extends ICommand, IGuiceAware {

	IPxDocGenerator getPxDocGenerator();
	
	void setStylesheetRegistry(IStylesheetsRegistry reg);
	
	void setResourceProvider(IResourceProvider provider);

	void setUiActivator(AbstractPxUiPlugin uiActivator);
	
}
