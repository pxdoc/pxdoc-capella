package org.pragmaticmodeling.pxdoc.runtime.ui.capella;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;

public class DefaultCapellaModelProvider implements IModelProvider {

	@Override
	public Object adaptSelection(Object selection) {
		return resolveSemanticObject(selection);
	}
	
	public Collection<EObject> getSemanticObjects(Collection<?> elements) {
		Collection<EObject> result = new ArrayList<EObject>();
		for (Object object : elements) {
			EObject semantic = resolveSemanticObject(object);
			if (semantic != null) {
				result.add(semantic);
			}
		}
		return result;
	}
	
	protected EObject resolveSemanticObject(Object object) {
		EObject semantic = null;

		if (object != null) {
			if (object instanceof EObject) {
				semantic = (EObject) object;

			} else if (object instanceof IAdaptable) {
				Object adapter = ((IAdaptable) object).getAdapter(EObject.class);
				if (adapter instanceof EObject) {
					semantic = (EObject) adapter;
				}
			}
		}

		if (semantic != null) {
			if (semantic instanceof DSemanticDecorator) {
				Object adapter = ((DSemanticDecorator) semantic).getTarget();
				if (adapter instanceof EObject) {
					semantic = (EObject) adapter;
				}
			}
		}

		return semantic;
	}

}
