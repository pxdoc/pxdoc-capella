package org.pragmaticmodeling.pxdoc.capella.examples.fullmodel.gen;

import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Underline;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.sirius.diagram.DSemanticDiagram;
import org.polarsys.capella.common.data.modellingcore.ModellingcorePackage;
import org.polarsys.capella.common.ui.providers.MDEAdapterFactoryLabelProvider;
import org.polarsys.capella.core.data.capellacore.CapellacorePackage;
import org.polarsys.capella.core.data.capellacore.Namespace;
import org.polarsys.capella.core.data.information.InformationPackage;
import org.polarsys.capella.core.data.oa.OperationalActivity;
import org.polarsys.capella.core.data.requirement.Requirement;
import org.polarsys.capella.core.model.handler.provider.CapellaAdapterFactoryProvider;
import org.polarsys.capella.core.semantic.queries.sirius.diagram.ModelElementRelatedDiagramsQuery;
import org.pragmaticmodeling.pxdoc.capella.common.CapellaDescriptionProvider;
import org.pragmaticmodeling.pxdoc.capella.common.Requirements;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.DocKind;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.runtime.capella.CapellaDiagramsProvider;

@SuppressWarnings("all")
public class FullModelExample extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public Requirements _Requirements = (Requirements)getGenerator().getModule(Requirements.class);
  
  public void addVisitedRequirement(final Requirement req) {
    _Requirements.addVisitedRequirement(req);
  }
  
  public List<Requirement> getVisitedRequirements() {
    return _Requirements.getVisitedRequirements();
  }
  
  public FullModelExample() {
    super();
  }
  
  public String getName() {
    return "FullModelExample";
  }
  
  public void generate() throws PxDocGenerationException {
    org.polarsys.capella.core.data.capellacore.Namespace model = (org.polarsys.capella.core.data.capellacore.Namespace)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting FullModelExample generation...");
    	checkStylesheet(getStylesheetName());
    	PxDocLauncherHelper.addStyleBinding(this, "BulletList", "ListParagraph");
    	PxDocLauncherHelper.addStyleBinding(this, "BodyText", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "Figure", "Caption");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqBody", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqId", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqTitle", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "Emphasis", "Quote");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqRationale", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqVerificationMethod", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqVerificationPhase", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "ReqVersion", "Normal");
    	main(null, model);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: FullModelExample generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "FullModelExampleStylesheet";
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Demonstrates how to mix/reuse various way of presenting elements.
   * For each type of object, a specific template is applied
   */
  public void description(final ContainerElement parent, final Namespace element, final DocKind strategy, final int level) throws PxDocGenerationException {
    boolean _matched = false;
    if (element instanceof Requirement) {
      _matched=true;
      _Requirements.requirement(parent, ((Requirement)element));
    }
    if (!_matched) {
      if (element instanceof OperationalActivity) {
        _matched=true;
        _EmfServices.docWithText(parent, ((OperationalActivity)element), level, false);
      }
    }
    if (!_matched) {
      {
        _EmfServices.genericDoc(parent, element, strategy, level, false);
        StyleApplication lObj0 = createStyleApplication(parent, false, "Subtitle", null, null, null);
        createText(lObj0, "All Related Diagrams");
        ModelElementRelatedDiagramsQuery query = new ModelElementRelatedDiagramsQuery();
        List<Object> _compute = query.compute(element);
        for (final Object d : _compute) {
          if ((d instanceof DSemanticDiagram)) {
            Hyperlink lObj1 = createHyperlink(parent, getBookmarkString(this.validBookmark(d)), null, null);
            createText(lObj1, ((DSemanticDiagram)d).getName());
            createParagraphBreak(parent);
          }
        }
      }
    }
    Iterator<org.polarsys.capella.core.data.capellacore.Namespace> lObj2 = getIterator(Iterables.<Namespace>filter(element.eContents(), Namespace.class));
    while (lObj2.hasNext()) {
      description(parent, lObj2.next(), strategy, (level + 1));
      
    }
  }
  
  /**
   * Entry point.
   */
  public void main(final ContainerElement parent, final Namespace model) throws PxDocGenerationException {
    String _xifexpression = null;
    Object _property = this.getProperty("title");
    boolean _tripleEquals = (_property == null);
    if (_tripleEquals) {
      String _text = this.getText(model);
      _xifexpression = ("Specification of " + _text);
    } else {
      Object _property_1 = this.getProperty("title");
      _xifexpression = ((String) _property_1);
    }
    String specTitle = _xifexpression;
    Object _property_2 = this.getProperty("society");
    String societyName = ((String) _property_2);
    Object _property_3 = this.getProperty("version");
    String version = ((String) _property_3);
    Object _property_4 = this.getProperty("generationStrategy");
    DocKind strategy = ((DocKind) _property_4);
    Object _property_5 = this.getProperty("suggestImprovements");
    Boolean suggest = ((Boolean) _property_5);
    this.setSuggestImprovements((suggest).booleanValue());
    CapellaDescriptionProvider _capellaDescriptionProvider = new CapellaDescriptionProvider(this);
    this.setDescriptionProvider(_capellaDescriptionProvider);
    CapellaDiagramsProvider _capellaDiagramsProvider = new CapellaDiagramsProvider();
    this.setDiagramProvider(_capellaDiagramsProvider);
    AdapterFactory _adapterFactory = CapellaAdapterFactoryProvider.getInstance().getAdapterFactory();
    MDEAdapterFactoryLabelProvider _mDEAdapterFactoryLabelProvider = new MDEAdapterFactoryLabelProvider(_adapterFactory);
    this.setLabelProvider(_mDEAdapterFactoryLabelProvider);
    Set<EStructuralFeature> _excludedFeatures = this.getExcludedFeatures();
    _excludedFeatures.add(ModellingcorePackage.Literals.ABSTRACT_NAMED_ELEMENT__NAME);
    Set<EStructuralFeature> _excludedFeatures_1 = this.getExcludedFeatures();
    _excludedFeatures_1.add(ModellingcorePackage.Literals.MODEL_ELEMENT__ID);
    Set<EStructuralFeature> _excludedFeatures_2 = this.getExcludedFeatures();
    _excludedFeatures_2.add(ModellingcorePackage.Literals.MODEL_ELEMENT__SID);
    Set<EStructuralFeature> _excludedFeatures_3 = this.getExcludedFeatures();
    _excludedFeatures_3.add(ModellingcorePackage.Literals.PUBLISHABLE_ELEMENT__VISIBLE_IN_DOC);
    Set<EStructuralFeature> _excludedFeatures_4 = this.getExcludedFeatures();
    _excludedFeatures_4.add(ModellingcorePackage.Literals.PUBLISHABLE_ELEMENT__VISIBLE_IN_LM);
    Set<EStructuralFeature> _excludedFeatures_5 = this.getExcludedFeatures();
    _excludedFeatures_5.add(ModellingcorePackage.Literals.FINALIZABLE_ELEMENT__FINAL);
    Set<EStructuralFeature> _excludedFeatures_6 = this.getExcludedFeatures();
    _excludedFeatures_6.add(InformationPackage.Literals.PROPERTY__IS_PART_OF_KEY);
    Set<EStructuralFeature> _excludedFeatures_7 = this.getExcludedFeatures();
    _excludedFeatures_7.add(InformationPackage.Literals.PROPERTY__IS_DERIVED);
    Set<EStructuralFeature> _excludedFeatures_8 = this.getExcludedFeatures();
    _excludedFeatures_8.add(InformationPackage.Literals.PROPERTY__AGGREGATION_KIND);
    Set<EStructuralFeature> _excludedFeatures_9 = this.getExcludedFeatures();
    _excludedFeatures_9.add(InformationPackage.Literals.PROPERTY__IS_READ_ONLY);
    Set<EStructuralFeature> _excludedFeatures_10 = this.getExcludedFeatures();
    _excludedFeatures_10.add(InformationPackage.Literals.MULTIPLICITY_ELEMENT__ORDERED);
    Set<EStructuralFeature> _excludedFeatures_11 = this.getExcludedFeatures();
    _excludedFeatures_11.add(InformationPackage.Literals.MULTIPLICITY_ELEMENT__UNIQUE);
    Set<EStructuralFeature> _excludedFeatures_12 = this.getExcludedFeatures();
    _excludedFeatures_12.add(InformationPackage.Literals.MULTIPLICITY_ELEMENT__MAX_INCLUSIVE);
    Set<EStructuralFeature> _excludedFeatures_13 = this.getExcludedFeatures();
    _excludedFeatures_13.add(InformationPackage.Literals.MULTIPLICITY_ELEMENT__MIN_INCLUSIVE);
    Set<EStructuralFeature> _excludedFeatures_14 = this.getExcludedFeatures();
    _excludedFeatures_14.add(CapellacorePackage.Literals.CAPELLA_ELEMENT__DESCRIPTION);
    Set<EStructuralFeature> _excludedFeatures_15 = this.getExcludedFeatures();
    _excludedFeatures_15.add(CapellacorePackage.Literals.FEATURE__IS_STATIC);
    Set<EStructuralFeature> _excludedFeatures_16 = this.getExcludedFeatures();
    _excludedFeatures_16.add(CapellacorePackage.Literals.FEATURE__VISIBILITY);
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, "FullModelExampleStylesheet", null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    setDocumentVariable(lObj0, "version", version);
    setDocumentProperty(lObj0, "title", specTitle);
    setDocumentProperty(lObj0, "Company", societyName);
    addDocument(lObj0);
    {
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      createParagraphBreak(lObj0);
      StyleApplication lObj1 = createStyleApplication(lObj0, false, "Title", null, null, null);
      {
        createText(lObj1, "Full description of ");
        createLineBreak(lObj1);
        createText(lObj1, specTitle);
      }
      createParagraphBreak(lObj0);
      StyleApplication lObj2 = createStyleApplication(lObj0, false, "Normal", AlignmentType.CENTER, null, null);
      {
        createImage(lObj2, "images/companyLogo.png", null, null, null, null, null);
        createParagraphBreak(lObj2);
        Italic lObj3 = createItalic(lObj2);
        createText(lObj3, societyName);
        createParagraphBreak(lObj2);
        createParagraphBreak(lObj2);
        Underline lObj4 = createUnderline(lObj2, UnderlineType.SINGLE);
        {
          createText(lObj4, "Version: ");
          createText(lObj4, version);
        }
      }
      createPageBreak(lObj0);
      createToc(lObj0, "Table of Content", "", "Title");
      createParagraphBreak(lObj0);
      createPageBreak(lObj0);
      boolean _isEmptyDocumentation = this.isEmptyDocumentation(model.getDescription());
      boolean _not = (!_isEmptyDocumentation);
      if (_not) {
        StyleApplication lObj5 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
        {
          createText(lObj5, "Overview of ");
          createText(lObj5, model.getName());
        }
        _CommonServices.optionalDocumentation(lObj0, model);
      }
      Iterator<org.polarsys.capella.core.data.capellacore.Namespace> lObj6 = getIterator(Iterables.<Namespace>filter(model.eContents(), Namespace.class));
      while (lObj6.hasNext()) {
        description(lObj0, lObj6.next(), strategy, 1);
        
      }
      boolean _isEmpty = this.getVisitedRequirements().isEmpty();
      boolean _not_1 = (!_isEmpty);
      if (_not_1) {
        StyleApplication lObj7 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
        createText(lObj7, "Requirements Traceability");
        _Requirements.reqToCapellaElementsMatrix(lObj0, model, this.getVisitedRequirements());
      }
    }
  }
}
