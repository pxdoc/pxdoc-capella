package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.polarsys.capella.core.data.la.LogicalArchitecture;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractLAExamplePropertyTester extends DefaultPropertyTester {

	public AbstractLAExamplePropertyTester() {
		super(LogicalArchitecture.class);
	}
}

