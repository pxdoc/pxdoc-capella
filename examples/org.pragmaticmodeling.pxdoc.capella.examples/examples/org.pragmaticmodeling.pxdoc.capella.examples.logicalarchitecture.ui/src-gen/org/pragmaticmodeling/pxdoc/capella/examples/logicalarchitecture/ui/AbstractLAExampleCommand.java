package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;

import java.util.Collection;

import org.pragmaticmodeling.pxdoc.runtime.ui.capella.CommandHandler;
import org.pragmaticmodeling.pxdoc.runtime.ui.capella.IPxDocCommand;

public abstract class AbstractLAExampleCommand extends CommandHandler {
	
	@Override
	protected IPxDocCommand createCommand(Collection<?> selection) {
		return new LAExampleCapellaCommand(selection.iterator().next());
	}
	
}

