package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;

import org.polarsys.capella.core.data.la.LogicalArchitecture;

/**
 * PxDocModel property tester and model provider
 */
public class LAExamplePropertyTester extends AbstractLAExamplePropertyTester {
	
	
	/** 
	 * test method overridden to enable the pxDoc menu only for LogicalArchitecture instances.
	 * 
	 * 
	 */
	
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {	
		return receiver instanceof LogicalArchitecture;
	}
}

