package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractLAExampleModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(LAExample.class);
	}

}