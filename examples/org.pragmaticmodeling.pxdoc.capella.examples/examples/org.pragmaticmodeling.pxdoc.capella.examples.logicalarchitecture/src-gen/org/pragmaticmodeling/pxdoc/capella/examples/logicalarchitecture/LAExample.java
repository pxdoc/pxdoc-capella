package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import fr.pragmaticmodeling.pxdoc.runtime.util.PxDocLauncherHelper;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.polarsys.capella.common.data.activity.InputPin;
import org.polarsys.capella.common.data.activity.OutputPin;
import org.polarsys.capella.core.data.capellacore.NamedElement;
import org.polarsys.capella.core.data.capellacore.Type;
import org.polarsys.capella.core.data.cs.Component;
import org.polarsys.capella.core.data.cs.Interface;
import org.polarsys.capella.core.data.cs.SystemComponentCapabilityRealizationInvolvement;
import org.polarsys.capella.core.data.fa.AbstractFunction;
import org.polarsys.capella.core.data.fa.ComponentExchange;
import org.polarsys.capella.core.data.fa.ComponentPort;
import org.polarsys.capella.core.data.fa.FunctionalExchange;
import org.polarsys.capella.core.data.information.Collection;
import org.polarsys.capella.core.data.information.DataPkg;
import org.polarsys.capella.core.data.information.ExchangeItem;
import org.polarsys.capella.core.data.information.ExchangeItemElement;
import org.polarsys.capella.core.data.information.Property;
import org.polarsys.capella.core.data.information.datatype.DataType;
import org.polarsys.capella.core.data.la.LogicalArchitecture;
import org.polarsys.capella.core.data.la.LogicalComponent;
import org.polarsys.capella.core.data.la.LogicalFunction;
import org.pragmaticmodeling.pxdoc.capella.common.CapellaDescriptionProvider;
import org.pragmaticmodeling.pxdoc.capella.common.CapellaDiagrams;
import org.pragmaticmodeling.pxdoc.capella.common.PxLogicalAnalysis;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.runtime.capella.CapellaDiagramsProvider;
import org.pragmaticmodeling.pxdoc.runtime.capella.ICapellaDiagramQueryBuilder;

@SuppressWarnings("all")
public class LAExample extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public CapellaDiagrams _CapellaDiagrams = (CapellaDiagrams)getGenerator().getModule(CapellaDiagrams.class);
  
  public ICapellaDiagramQueryBuilder queryCapellaDiagrams(final Object namespace) {
    return _CapellaDiagrams.queryCapellaDiagrams(namespace);
  }
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public PxLogicalAnalysis _PxLogicalAnalysis = (PxLogicalAnalysis)getGenerator().getModule(PxLogicalAnalysis.class);
  
  public String getCeDirection(final ComponentExchange ce, final Component sourceComponent) {
    return _PxLogicalAnalysis.getCeDirection(ce, sourceComponent);
  }
  
  public String getDestinationComponent(final ComponentExchange ce, final Component sourceComponent) {
    return _PxLogicalAnalysis.getDestinationComponent(ce, sourceComponent);
  }
  
  public LAExample() {
    super();
  }
  
  public String getName() {
    return "LAExample";
  }
  
  public void generate() throws PxDocGenerationException {
    org.polarsys.capella.core.data.la.LogicalArchitecture la = (org.polarsys.capella.core.data.la.LogicalArchitecture)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting LAExample generation...");
    	checkStylesheet(getStylesheetName());
    	PxDocLauncherHelper.addStyleBinding(this, "BodyText", "Normal");
    	PxDocLauncherHelper.addStyleBinding(this, "BulletList", "ListParagraph");
    	PxDocLauncherHelper.addStyleBinding(this, "Emphasis", "Quote");
    	PxDocLauncherHelper.addStyleBinding(this, "Figure", "Caption");
    	main(null, la);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: LAExample generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "CapellaStylesheet";
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void visibleDiagrams(final ContainerElement parent, final EObject pNamespace) throws PxDocGenerationException {
    List<Diagram> diagrams = this.queryCapellaDiagrams(pNamespace).visibleInDoc().execute();
    Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj0 = getIterator(diagrams);
    while (lObj0.hasNext()) {
      _DiagramServices.includeDiagram(parent, lObj0.next());
      
    }
  }
  
  public void interfaceDataDescription(final ContainerElement parent, final DataPkg dp, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    createText(lObj0, dp.getName());
    _CommonServices.documentation(parent, dp);
    visibleDiagrams(parent, dp);
    Font lObj1 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj1, "Exchange items definition: ");
    createParagraphBreak(parent);
    EList<ExchangeItem> _ownedExchangeItems = dp.getOwnedExchangeItems();
    for (final ExchangeItem ex : _ownedExchangeItems) {
      {
        String lObj2 = ex.getId();
        Bookmark lObj3 = createBookmark(parent, getBookmarkString(lObj2));
        StyleApplication lObj4 = createStyleApplication(lObj3, false, "Subtitle", null, null, null);
        createText(lObj4, ex.getName());
        int _size = ex.getOwnedElements().size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          Table lObj5 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
          {
            Row lObj6 = createRow(lObj5, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
            {
              Cell lObj7 = createCell(lObj6);
              createText(lObj7, "Owned element");
              Cell lObj8 = createCell(lObj6);
              createText(lObj8, "Element type");
            }
            EList<ExchangeItemElement> _ownedElements = ex.getOwnedElements();
            for (final ExchangeItemElement el : _ownedElements) {
              Row lObj9 = createRow(lObj5, null, false, null, ShadingPattern.NO_VALUE, false, null);
              {
                Cell lObj10 = createCell(lObj9);
                createText(lObj10, el.getName());
                Cell lObj11 = createCell(lObj9);
                createText(lObj11, el.getType().getName());
              }
            }
          }
        } else {
          Italic lObj12 = createItalic(parent);
          createText(lObj12, "No contained elements");
        }
      }
    }
    createParagraphBreak(parent);
    int _size = dp.getOwnedCollections().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      Font lObj13 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
      createText(lObj13, "Collections definition:");
      Table lObj14 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj15 = createRow(lObj14, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj16 = createCell(lObj15);
          createText(lObj16, "Collection");
          Cell lObj17 = createCell(lObj15);
          createText(lObj17, "Collection type");
        }
        EList<Collection> _ownedCollections = dp.getOwnedCollections();
        for (final Collection coll : _ownedCollections) {
          Row lObj18 = createRow(lObj14, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj19 = createCell(lObj18);
            createText(lObj19, coll.getName());
            Cell lObj20 = createCell(lObj18);
            createText(lObj20, coll.getType().getName());
          }
        }
      }
    }
    createParagraphBreak(parent);
    int _size_1 = dp.getOwnedClasses().size();
    boolean _greaterThan_1 = (_size_1 > 0);
    if (_greaterThan_1) {
      Font lObj21 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
      createText(lObj21, "Classes definition:");
      EList<org.polarsys.capella.core.data.information.Class> _ownedClasses = dp.getOwnedClasses();
      for (final org.polarsys.capella.core.data.information.Class cl : _ownedClasses) {
        {
          StyleApplication lObj22 = createStyleApplication(parent, false, "Subtitle", null, null, null);
          createText(lObj22, cl.getName());
          int _size_2 = cl.getContainedProperties().size();
          boolean _greaterThan_2 = (_size_2 > 0);
          if (_greaterThan_2) {
            Table lObj23 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
            {
              Row lObj24 = createRow(lObj23, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
              {
                Cell lObj25 = createCell(lObj24);
                createText(lObj25, "Contained property");
                Cell lObj26 = createCell(lObj24);
                createText(lObj26, "Property type");
              }
              EList<Property> _containedProperties = cl.getContainedProperties();
              for (final Property p : _containedProperties) {
                Row lObj27 = createRow(lObj23, null, false, null, ShadingPattern.NO_VALUE, false, null);
                {
                  Cell lObj28 = createCell(lObj27);
                  createText(lObj28, p.getName());
                  Cell lObj29 = createCell(lObj27);
                  Type _type = p.getType();
                  boolean _tripleNotEquals = (_type != null);
                  if (_tripleNotEquals) {
                    createText(lObj29, p.getType().getName());
                  }
                }
              }
            }
          } else {
            Italic lObj30 = createItalic(parent);
            createText(lObj30, "No contained property");
          }
        }
      }
    }
    createParagraphBreak(parent);
    int _size_2 = dp.getOwnedDataTypes().size();
    boolean _greaterThan_2 = (_size_2 > 0);
    if (_greaterThan_2) {
      Font lObj31 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
      createText(lObj31, "Data types:");
      EList<DataType> _ownedDataTypes = dp.getOwnedDataTypes();
      for (final DataType dt : _ownedDataTypes) {
        StyleApplication lObj32 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
        createText(lObj32, dt.getName());
      }
    }
    createParagraphBreak(parent);
    Iterator<org.polarsys.capella.core.data.information.DataPkg> lObj33 = getIterator(dp.getOwnedDataPkgs());
    while (lObj33.hasNext()) {
      interfaceDataDescription(parent, lObj33.next(), (level + 1));
      
    }
  }
  
  public void listItem(final ContainerElement parent, final ExchangeItem exchangedItems) throws PxDocGenerationException {
  }
  
  public void funcExchRow(final ContainerElement parent, final FunctionalExchange fe) throws PxDocGenerationException {
    Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
    {
      Cell lObj1 = createCell(lObj0);
      createText(lObj1, fe.getName());
      Cell lObj2 = createCell(lObj0);
      Iterator<org.polarsys.capella.core.data.information.ExchangeItem> lObj3 = getIterator(fe.getExchangedItems());
      while (lObj3.hasNext()) {
        listItem(lObj2, lObj3.next());
        
      }
    }
  }
  
  /**
   * Template for the Logical Function description
   */
  public void logicalFunctionDescription(final ContainerElement parent, final LogicalFunction lf, final String topComponentID, final String topComponentName, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    String lObj1 = lf.getId();
    Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
    _EmfServices.iconAndText(lObj2, lf);
    Font lObj3 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj3, "Description: ");
    _CommonServices.documentation(parent, lf);
    visibleDiagrams(parent, lf);
    createParagraphBreak(parent);
    createParagraphBreak(parent);
    Font lObj4 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj4, "Parent Logical Function: ");
    boolean _equals = Objects.equal(topComponentID, "TopLevelComponent");
    if (_equals) {
      StyleApplication lObj5 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
      Italic lObj6 = createItalic(lObj5);
      createText(lObj6, topComponentName);
    } else {
      createParagraphBreak(parent);
      StyleApplication lObj7 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
      Hyperlink lObj8 = createHyperlink(lObj7, getBookmarkString(topComponentID), null, null);
      _EmfServices.iconAndText(lObj8, lf, topComponentName);
    }
    createParagraphBreak(parent);
    Font lObj9 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj9, "Children Logical Functions: ");
    int _size = lf.getChildrenLogicalFunctions().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      createParagraphBreak(parent);
      EList<LogicalFunction> _childrenLogicalFunctions = lf.getChildrenLogicalFunctions();
      for (final LogicalFunction f : _childrenLogicalFunctions) {
        StyleApplication lObj10 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
        Hyperlink lObj11 = createHyperlink(lObj10, getBookmarkString(f.getId()), null, null);
        _EmfServices.iconAndText(lObj11, f);
      }
    } else {
      Italic lObj12 = createItalic(parent);
      createText(lObj12, "No children function");
    }
    createParagraphBreak(parent);
    Font lObj13 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj13, "List of inputs: ");
    int _size_1 = lf.getInputs().size();
    boolean _greaterThan_1 = (_size_1 > 0);
    if (_greaterThan_1) {
      createParagraphBreak(parent);
      Table lObj14 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj15 = createRow(lObj14, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj16 = createCell(lObj15);
          createText(lObj16, "Incoming functional exchange");
          Cell lObj17 = createCell(lObj15);
          createText(lObj17, "Exchanged items");
        }
        EList<InputPin> _inputs = lf.getInputs();
        for (final InputPin i : _inputs) {
          Iterator<org.polarsys.capella.core.data.fa.FunctionalExchange> lObj18 = getIterator(Iterables.<FunctionalExchange>filter(i.getIncoming(), FunctionalExchange.class));
          while (lObj18.hasNext()) {
            funcExchRow(lObj14, lObj18.next());
            
          }
        }
      }
      createParagraphBreak(parent);
    } else {
      Italic lObj19 = createItalic(parent);
      createText(lObj19, "No input");
      createParagraphBreak(parent);
    }
    createParagraphBreak(parent);
    createParagraphBreak(parent);
    Font lObj20 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj20, "List of outputs: ");
    int _size_2 = lf.getOutputs().size();
    boolean _greaterThan_2 = (_size_2 > 0);
    if (_greaterThan_2) {
      createParagraphBreak(parent);
      Table lObj21 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj22 = createRow(lObj21, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj23 = createCell(lObj22);
          createText(lObj23, "Outgoing functional exchange");
          Cell lObj24 = createCell(lObj22);
          createText(lObj24, "Exchanged items");
        }
        EList<OutputPin> _outputs = lf.getOutputs();
        for (final OutputPin i : _outputs) {
          Iterator<org.polarsys.capella.core.data.fa.FunctionalExchange> lObj25 = getIterator(Iterables.<FunctionalExchange>filter(i.getOutgoing(), FunctionalExchange.class));
          while (lObj25.hasNext()) {
            funcExchRow(lObj21, lObj25.next());
            
          }
        }
      }
    } else {
      Italic lObj26 = createItalic(parent);
      createText(lObj26, "No output");
    }
    createParagraphBreak(parent);
    createParagraphBreak(parent);
    Iterator<org.polarsys.capella.core.data.la.LogicalFunction> lObj27 = getIterator(lf.getContainedLogicalFunctions());
    while (lObj27.hasNext()) {
      logicalFunctionDescription(parent, lObj27.next(), lf.getId(), lf.getName(), (level + 1));
      
    }
  }
  
  /**
   * Template for the Logical Component Description
   */
  public void logicalComponentsDecription(final ContainerElement parent, final LogicalComponent lc, final String modelDescription, final String topComponentID, final String topComponentName, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    String lObj1 = lc.getId();
    Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
    _EmfServices.iconAndText(lObj2, lc);
    visibleDiagrams(parent, lc);
    Font lObj3 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj3, "Parent Logical Component: ");
    createText(parent, " ");
    boolean _equals = Objects.equal(topComponentID, "TopLevelComponent");
    if (_equals) {
      StyleApplication lObj4 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
      Italic lObj5 = createItalic(lObj4);
      createText(lObj5, topComponentName);
    } else {
      StyleApplication lObj6 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
      Hyperlink lObj7 = createHyperlink(lObj6, getBookmarkString(topComponentID), null, null);
      _EmfServices.iconAndText(lObj7, lc, topComponentName);
    }
    createParagraphBreak(parent);
    Font lObj8 = createFont(parent, null, null, true, false, true, UnderlineType.SINGLE, false, null, null, null);
    createText(lObj8, "Description: ");
    _CommonServices.documentation(parent, lc);
    boolean _isEmpty = lc.getParticipationsInCapabilityRealizations().isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj9 = createStyleApplication(parent, false, "Subtitle", null, null, null);
      createText(lObj9, "Participations in Capability Realizations");
      EList<SystemComponentCapabilityRealizationInvolvement> _participationsInCapabilityRealizations = lc.getParticipationsInCapabilityRealizations();
      for (final SystemComponentCapabilityRealizationInvolvement picr : _participationsInCapabilityRealizations) {
        StyleApplication lObj10 = createStyleApplication(parent, false, "ListParagraph", null, null, null);
        _EmfServices.iconAndText(lObj10, picr, ((NamedElement) picr.eContainer()).getName());
      }
    }
    int _size = lc.getAllocatedFunctions().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      StyleApplication lObj11 = createStyleApplication(parent, false, "Subtitle", null, null, null);
      createText(lObj11, "Allocated Functions");
      Table lObj12 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth13 = createMeasure((float)25, UnitKind.PC);
      createCellDef(lObj12, null, cellWidth13, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth14 = createMeasure((float)75, UnitKind.PC);
      createCellDef(lObj12, null, cellWidth14, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj15 = createRow(lObj12, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj16 = createCell(lObj15);
          createText(lObj16, "Function name");
          Cell lObj17 = createCell(lObj15);
          createText(lObj17, "Description");
        }
        EList<AbstractFunction> _allocatedFunctions = lc.getAllocatedFunctions();
        for (final AbstractFunction af : _allocatedFunctions) {
          Row lObj18 = createRow(lObj12, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj19 = createCell(lObj18);
            _EmfServices.iconAndText(lObj19, af);
            Cell lObj20 = createCell(lObj18);
            addAllToParent(getLanguageRenderer("html").render(lObj20, af.getDescription(), null), lObj20);
            
          }
        }
      }
    }
    int _size_1 = lc.getContainedComponentPorts().size();
    boolean _greaterThan_1 = (_size_1 > 0);
    if (_greaterThan_1) {
      StyleApplication lObj21 = createStyleApplication(parent, false, "Subtitle", null, null, null);
      createText(lObj21, "Component Exchanges");
      Table lObj22 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth23 = createMeasure((float)15, UnitKind.PC);
      createCellDef(lObj22, null, cellWidth23, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth24 = createMeasure((float)15, UnitKind.PC);
      createCellDef(lObj22, null, cellWidth24, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth25 = createMeasure((float)15, UnitKind.PC);
      createCellDef(lObj22, null, cellWidth25, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth26 = createMeasure((float)55, UnitKind.PC);
      createCellDef(lObj22, null, cellWidth26, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj27 = createRow(lObj22, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj28 = createCell(lObj27);
          createText(lObj28, "Component Exchange Name");
          Cell lObj29 = createCell(lObj27);
          createText(lObj29, "Direction");
          Cell lObj30 = createCell(lObj27);
          createText(lObj30, "Destination Component");
          Cell lObj31 = createCell(lObj27);
          createText(lObj31, "Description");
        }
        EList<ComponentPort> _containedComponentPorts = lc.getContainedComponentPorts();
        for (final ComponentPort ce : _containedComponentPorts) {
          EList<ComponentExchange> _componentExchanges = ce.getComponentExchanges();
          for (final ComponentExchange cee : _componentExchanges) {
            Row lObj32 = createRow(lObj22, null, false, null, ShadingPattern.NO_VALUE, false, null);
            {
              Cell lObj33 = createCell(lObj32);
              createText(lObj33, cee.getName());
              Cell lObj34 = createCell(lObj32);
              createText(lObj34, ce.getOrientation());
              Cell lObj35 = createCell(lObj32);
              createText(lObj35, this.getDestinationComponent(cee, lc));
              Cell lObj36 = createCell(lObj32);
              _CommonServices.documentation(lObj36, ce);
            }
          }
        }
      }
    }
    int _size_2 = lc.getRequiredInterfaces().size();
    boolean _greaterThan_2 = (_size_2 > 0);
    if (_greaterThan_2) {
      StyleApplication lObj37 = createStyleApplication(parent, false, "Subtitle", null, null, null);
      createText(lObj37, "Required Interfaces");
      Table lObj38 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj39 = createRow(lObj38, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj40 = createCell(lObj39);
          createText(lObj40, "Interface name");
          Cell lObj41 = createCell(lObj39);
          createText(lObj41, "Description");
        }
        EList<Interface> _requiredInterfaces = lc.getRequiredInterfaces();
        for (final Interface ri : _requiredInterfaces) {
          Row lObj42 = createRow(lObj38, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj43 = createCell(lObj42);
            _EmfServices.iconAndText(lObj43, ri);
            Cell lObj44 = createCell(lObj42);
            addAllToParent(getLanguageRenderer("html").render(lObj44, ri.getDescription(), null), lObj44);
            
          }
        }
      }
    }
    int _size_3 = lc.getProvidedInterfaces().size();
    boolean _greaterThan_3 = (_size_3 > 0);
    if (_greaterThan_3) {
      StyleApplication lObj45 = createStyleApplication(parent, false, "Subtitle", null, null, null);
      createText(lObj45, "Required Interfaces");
      Table lObj46 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj47 = createRow(lObj46, null, true, "114,142,186", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj48 = createCell(lObj47);
          createText(lObj48, "Interface name");
          Cell lObj49 = createCell(lObj47);
          createText(lObj49, "Description");
        }
        EList<Interface> _providedInterfaces = lc.getProvidedInterfaces();
        for (final Interface pi : _providedInterfaces) {
          Row lObj50 = createRow(lObj46, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj51 = createCell(lObj50);
            _EmfServices.iconAndText(lObj51, pi);
            Cell lObj52 = createCell(lObj50);
            addAllToParent(getLanguageRenderer("html").render(lObj52, pi.getDescription(), null), lObj52);
            
          }
        }
      }
    }
    Iterator<org.polarsys.capella.core.data.la.LogicalComponent> lObj53 = getIterator(lc.getOwnedLogicalComponents());
    while (lObj53.hasNext()) {
      logicalComponentsDecription(parent, lObj53.next(), modelDescription, lc.getId(), lc.getName(), (level + 1));
      
    }
  }
  
  /**
   * Entry point: A Logical Architecture of the model
   * The pxDoc menu is only enabled on an instance of Logical Architecture: to prevent displaying the pxDoc menu elsewhere
   * we add to modify the @see org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui.LAExamplePropertyTester
   */
  public void main(final ContainerElement parent, final LogicalArchitecture la) throws PxDocGenerationException {
    CapellaDescriptionProvider _capellaDescriptionProvider = new CapellaDescriptionProvider(this);
    this.setDescriptionProvider(_capellaDescriptionProvider);
    CapellaDiagramsProvider _capellaDiagramsProvider = new CapellaDiagramsProvider();
    this.setDiagramProvider(_capellaDiagramsProvider);
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, "CapellaStylesheet", null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    addDocument(lObj0);
    {
      StyleApplication lObj1 = createStyleApplication(lObj0, false, "Title", AlignmentType.CENTER, null, null);
      {
        createParagraphBreak(lObj1);
        createParagraphBreak(lObj1);
        createParagraphBreak(lObj1);
        createText(lObj1, "Architecture File of ");
        createText(lObj1, la.getName());
      }
      createParagraphBreak(lObj0);
      StyleApplication lObj2 = createStyleApplication(lObj0, false, "Normal", AlignmentType.CENTER, null, null);
      createText(lObj2, "Document generated automatically by pxDoc from Capella model of the system");
      createPageBreak(lObj0);
      StyleApplication lObj3 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
      createText(lObj3, "Table of Content");
      createToc(lObj0, null, "", null);
      createParagraphBreak(lObj0);
      createPageBreak(lObj0);
      StyleApplication lObj4 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
      createText(lObj4, "Description of Logical Components");
      logicalComponentsDecription(lObj0, la.getOwnedLogicalComponent(), la.getDescription(), "TopLevelComponent", "This is a top level component", 2);
      StyleApplication lObj5 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
      createText(lObj5, "Descriptions of Logical Functions");
      Iterator<org.polarsys.capella.core.data.la.LogicalFunction> lObj6 = getIterator(la.getContainedLogicalFunctionPkg().getOwnedLogicalFunctions());
      while (lObj6.hasNext()) {
        logicalFunctionDescription(lObj0, lObj6.next(), "RootFunction", "This is the root function", 2);
        
      }
      StyleApplication lObj7 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
      createText(lObj7, "Descriptions of Interfaces and Data");
      interfaceDataDescription(lObj0, la.getOwnedDataPkg(), 2);
    }
  }
}
