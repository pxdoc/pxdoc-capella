package org.pragmaticmodeling.pxdoc.capella.examples.fullmodel.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.polarsys.capella.core.data.capellacore.Namespace;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractFullModelExamplePropertyTester extends DefaultPropertyTester {

	public AbstractFullModelExamplePropertyTester() {
		super(Namespace.class);
	}
}

