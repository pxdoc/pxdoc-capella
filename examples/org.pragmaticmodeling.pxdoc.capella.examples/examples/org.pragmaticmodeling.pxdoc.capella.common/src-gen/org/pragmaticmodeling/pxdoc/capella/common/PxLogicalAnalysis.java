package org.pragmaticmodeling.pxdoc.capella.common;

import com.google.common.base.Objects;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;
import org.polarsys.capella.core.data.cs.Component;
import org.polarsys.capella.core.data.fa.ComponentExchange;
import org.polarsys.capella.core.data.fa.ComponentPort;
import org.polarsys.capella.core.data.information.Port;

@SuppressWarnings("all")
public class PxLogicalAnalysis extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public PxLogicalAnalysis(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public String getDestinationComponent(final ComponentExchange ce, final Component sourceComponent) {
    EObject _eContainer = ce.getSourcePort().eContainer();
    Component sourceCeComponent = ((Component) _eContainer);
    EObject _eContainer_1 = ce.getTargetPort().eContainer();
    Component targetCeComponent = ((Component) _eContainer_1);
    Port port = null;
    boolean _equals = Objects.equal(sourceCeComponent, sourceComponent);
    if (_equals) {
      port = ce.getTargetPort();
    } else {
      boolean _equals_1 = Objects.equal(targetCeComponent, sourceComponent);
      if (_equals_1) {
        port = ce.getSourcePort();
      }
    }
    EObject _eContainer_2 = port.eContainer();
    return ((Component) _eContainer_2).getName();
  }
  
  public String getCeDirection(final ComponentExchange ce, final Component sourceComponent) {
    EObject _eContainer = ce.getSourcePort().eContainer();
    Component sourceCeComponent = ((Component) _eContainer);
    EObject _eContainer_1 = ce.getTargetPort().eContainer();
    Component targetCeComponent = ((Component) _eContainer_1);
    Port port = null;
    boolean _equals = Objects.equal(sourceCeComponent, sourceComponent);
    if (_equals) {
      port = ce.getSourcePort();
    } else {
      boolean _equals_1 = Objects.equal(targetCeComponent, sourceComponent);
      if (_equals_1) {
        port = ce.getTargetPort();
      }
    }
    if ((port instanceof ComponentPort)) {
      return ((ComponentPort)port).getOrientation().toString();
    }
    return "";
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
}
