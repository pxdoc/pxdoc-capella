package org.pragmaticmodeling.pxdoc.capella.common;

import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.BorderType;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.Color;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.MergeKind;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.polarsys.capella.core.data.capellacore.CapellaElement;
import org.polarsys.capella.core.data.capellacore.NamedElement;
import org.polarsys.capella.core.data.capellacore.Namespace;
import org.polarsys.capella.core.data.requirement.Requirement;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;

@SuppressWarnings("all")
public class Requirements extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public List<Requirement> visitedRequirements = new ArrayList<Requirement>();
  
  public boolean isProductionMode = true;
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public Requirements(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public List<Requirement> getVisitedRequirements() {
    return this.visitedRequirements;
  }
  
  public void addVisitedRequirement(final Requirement req) {
    this.visitedRequirements.add(req);
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void reqToCERow(final ContainerElement parent, final CapellaElement element, final Requirement req) throws PxDocGenerationException {
    Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
    Measure cellWidth1 = createMeasure((float)50, UnitKind.PC);
    createCellDef(lObj0, MergeKind.AUTOMATIC, cellWidth1, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
    Measure cellWidth2 = createMeasure((float)50, UnitKind.PC);
    createCellDef(lObj0, null, cellWidth2, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
    {
      Cell lObj3 = createCell(lObj0);
      Hyperlink lObj4 = createHyperlink(lObj3, getBookmarkString(this.validBookmark(req.getId())), null, null);
      {
        createText(lObj4, req.getRequirementId());
        createText(lObj4, " ");
        createText(lObj4, req.getName());
      }
      Cell lObj5 = createCell(lObj0);
      Hyperlink lObj6 = createHyperlink(lObj5, getBookmarkString(this.validBookmark(element.getId())), null, null);
      _EmfServices.iconAndText(lObj6, ((NamedElement) element));
    }
  }
  
  public void reqToCERowBlock(final ContainerElement parent, final Requirement req) throws PxDocGenerationException {
    boolean _isEmpty = req.getRelatedCapellaElements().isEmpty();
    if (_isEmpty) {
      Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
      Measure cellWidth1 = createMeasure((float)50, UnitKind.PC);
      createCellDef(lObj0, null, cellWidth1, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth2 = createMeasure((float)50, UnitKind.PC);
      createCellDef(lObj0, null, cellWidth2, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Cell lObj3 = createCell(lObj0);
        Hyperlink lObj4 = createHyperlink(lObj3, getBookmarkString(this.validBookmark(req.getId())), null, null);
        {
          createText(lObj4, req.getRequirementId());
          createText(lObj4, " ");
          createText(lObj4, req.getName());
        }
        Cell lObj5 = createCell(lObj0);
        StyleApplication lObj6 = createStyleApplication(lObj5, false, getBindedStyle("BodyText"), AlignmentType.CENTER, null, null);
        boolean _suggestImprovements = this.getSuggestImprovements();
        if (_suggestImprovements) {
          Color lObj7 = createColor(lObj6, "255,0,0");
          createText(lObj7, "Not allocated.");
        } else {
          createText(lObj6, "Not allocated.");
        }
      }
    } else {
      Iterator<org.polarsys.capella.core.data.capellacore.CapellaElement> lObj8 = getIterator(req.getRelatedCapellaElements());
      while (lObj8.hasNext()) {
        reqToCERow(parent, lObj8.next(), req);
        
      }
    }
  }
  
  public void reqToCapellaElementsMatrix(final ContainerElement parent, final Namespace context, final List<Requirement> reqs) throws PxDocGenerationException {
    Table lObj0 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
    Measure cellWidth1 = createMeasure((float)50, UnitKind.PC);
    createCellDef(lObj0, null, cellWidth1, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
    Measure cellWidth2 = createMeasure((float)50, UnitKind.PC);
    createCellDef(lObj0, null, cellWidth2, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
    {
      Row lObj3 = createRow(lObj0, null, true, "199,197,237", ShadingPattern.NO_VALUE, false, null);
      {
        Cell lObj4 = createCell(lObj3);
        StyleApplication lObj5 = createStyleApplication(lObj4, false, getBindedStyle("BodyText"), AlignmentType.CENTER, null, null);
        Bold lObj6 = createBold(lObj5);
        createText(lObj6, "Requirement");
        Cell lObj7 = createCell(lObj3);
        StyleApplication lObj8 = createStyleApplication(lObj7, false, getBindedStyle("BodyText"), AlignmentType.CENTER, null, null);
        Bold lObj9 = createBold(lObj8);
        createText(lObj9, "Element");
      }
      Iterator<org.polarsys.capella.core.data.requirement.Requirement> lObj10 = getIterator(reqs);
      while (lObj10.hasNext()) {
        reqToCERowBlock(lObj0, lObj10.next());
        
      }
    }
  }
  
  public void requirement(final ContainerElement parent, final Requirement req) throws PxDocGenerationException {
    String _requirementId = req.getRequirementId();
    String _plus = ("Generating Requirement " + _requirementId);
    String _plus_1 = (_plus + "...");
    this.log(_plus_1);
    this.addVisitedRequirement(req);
    Table lObj0 = createTable(parent, null, null, createBorder(BorderType.NONE, null, -1), null, ShadingPattern.NO_VALUE);
    {
      Row lObj1 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
      Measure cellWidth2 = createMeasure((float)20, UnitKind.PC);
      createCellDef(lObj1, null, cellWidth2, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth3 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj1, null, cellWidth3, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth4 = createMeasure((float)10, UnitKind.PC);
      createCellDef(lObj1, null, cellWidth4, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Cell lObj5 = createCell(lObj1);
        StyleApplication lObj6 = createStyleApplication(lObj5, false, getBindedStyle("ReqId"), null, null, null);
        String lObj7 = this.validBookmark(req.getId());
        Bookmark lObj8 = createBookmark(lObj6, getBookmarkString(lObj7));
        createText(lObj8, req.getRequirementId());
        Cell lObj9 = createCell(lObj1);
        if ((this.getSuggestImprovements() && this.isEmptyDocumentation(req.getName()))) {
          StyleApplication lObj10 = createStyleApplication(lObj9, false, getBindedStyle("ReqTitle"), null, null, null);
          Color lObj11 = createColor(lObj10, "255,0,0");
          createText(lObj11, "A title shall be provided.");
        } else {
          StyleApplication lObj12 = createStyleApplication(lObj9, false, getBindedStyle("ReqTitle"), null, null, null);
          createText(lObj12, req.getName());
        }
        Cell lObj13 = createCell(lObj1);
        StyleApplication lObj14 = createStyleApplication(lObj13, false, getBindedStyle("ReqVersion"), null, null, null);
        createText(lObj14, req.getImplementationVersion());
      }
      Row lObj15 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
      Cell lObj16 = createCell(lObj15);
      if ((this.getSuggestImprovements() && this.isEmptyDocumentation(req.getDescription()))) {
        StyleApplication lObj17 = createStyleApplication(lObj16, false, getBindedStyle("ReqBody"), null, null, null);
        Color lObj18 = createColor(lObj17, "255,0,0");
        createText(lObj18, "A requirement description shall be provided.");
      } else {
        StyleApplication lObj19 = createStyleApplication(lObj16, false, getBindedStyle("ReqBody"), null, null, null);
        createText(lObj19, req.getDescription());
      }
      boolean _isEmptyDocumentation = this.isEmptyDocumentation(req.getAdditionalInformation());
      boolean _not = (!_isEmptyDocumentation);
      if (_not) {
        Row lObj20 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj21 = createCell(lObj20);
          Measure cellWidth22 = createMeasure((float)10, UnitKind.PC);
          createCellDef(lObj21, MergeKind.AUTOMATIC, cellWidth22, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          createText(lObj21, "");
          Cell lObj23 = createCell(lObj20);
          StyleApplication lObj24 = createStyleApplication(lObj23, false, getBindedStyle("ReqRationale"), null, null, null);
          createText(lObj24, req.getAdditionalInformation());
        }
      }
      boolean _isEmptyDocumentation_1 = this.isEmptyDocumentation(req.getVerificationPhase());
      boolean _not_1 = (!_isEmptyDocumentation_1);
      if (_not_1) {
        Row lObj25 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj26 = createCell(lObj25);
          Measure cellWidth27 = createMeasure((float)10, UnitKind.PC);
          createCellDef(lObj26, MergeKind.AUTOMATIC, cellWidth27, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          createText(lObj26, "");
          Cell lObj28 = createCell(lObj25);
          Measure cellWidth29 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj28, null, cellWidth29, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          StyleApplication lObj30 = createStyleApplication(lObj28, false, getBindedStyle("BodyText"), AlignmentType.RIGHT, null, null);
          Font lObj31 = createFont(lObj30, null, null, false, true, false, null, false, null, "128,128,128", null);
          createText(lObj31, "Verification Phase :");
          Cell lObj32 = createCell(lObj25);
          StyleApplication lObj33 = createStyleApplication(lObj32, false, getBindedStyle("ReqVerificationPhase"), null, null, null);
          createText(lObj33, req.getVerificationPhase());
        }
      }
      boolean _isEmptyDocumentation_2 = this.isEmptyDocumentation(req.getVerificationMethod());
      boolean _not_2 = (!_isEmptyDocumentation_2);
      if (_not_2) {
        Row lObj34 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj35 = createCell(lObj34);
          Measure cellWidth36 = createMeasure((float)10, UnitKind.PC);
          createCellDef(lObj35, MergeKind.AUTOMATIC, cellWidth36, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          createText(lObj35, "");
          Cell lObj37 = createCell(lObj34);
          Measure cellWidth38 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj37, null, cellWidth38, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          StyleApplication lObj39 = createStyleApplication(lObj37, false, getBindedStyle("BodyText"), AlignmentType.RIGHT, null, null);
          Font lObj40 = createFont(lObj39, null, null, false, true, false, null, false, null, "128,128,128", null);
          createText(lObj40, "Verification Method :");
          Cell lObj41 = createCell(lObj34);
          StyleApplication lObj42 = createStyleApplication(lObj41, false, getBindedStyle("ReqVerificationMethod"), null, null, null);
          createText(lObj42, req.getVerificationMethod());
        }
      }
    }
    createParagraphBreak(parent);
  }
}
