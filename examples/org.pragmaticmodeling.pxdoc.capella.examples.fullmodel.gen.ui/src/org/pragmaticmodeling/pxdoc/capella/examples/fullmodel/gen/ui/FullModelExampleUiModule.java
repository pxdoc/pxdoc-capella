package org.pragmaticmodeling.pxdoc.capella.examples.fullmodel.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public class FullModelExampleUiModule extends AbstractFullModelExampleUiModule {
		   
	public FullModelExampleUiModule(AbstractPxUiPlugin plugin) {
		super(plugin);
	}
	
}
	
