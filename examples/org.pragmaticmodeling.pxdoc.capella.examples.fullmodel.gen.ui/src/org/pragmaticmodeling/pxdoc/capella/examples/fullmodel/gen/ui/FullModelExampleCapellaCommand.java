package org.pragmaticmodeling.pxdoc.capella.examples.fullmodel.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.capella.AbstractCapellaWizardCommand;

public class FullModelExampleCapellaCommand extends AbstractCapellaWizardCommand {
	
	public FullModelExampleCapellaCommand(Object selection) {
		super(selection);
	}

}

