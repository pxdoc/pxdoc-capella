package org.pragmaticmodeling.pxdoc.capella.examples.fullmodel.gen.ui;

import java.util.Collection;

import org.pragmaticmodeling.pxdoc.runtime.ui.capella.CommandHandler;
import org.pragmaticmodeling.pxdoc.runtime.ui.capella.IPxDocCommand;

public abstract class AbstractFullModelExampleCommand extends CommandHandler {
	
	@Override
	protected IPxDocCommand createCommand(Collection<?> selection) {
		return new FullModelExampleCapellaCommand(selection.iterator().next());
	}
	
}

