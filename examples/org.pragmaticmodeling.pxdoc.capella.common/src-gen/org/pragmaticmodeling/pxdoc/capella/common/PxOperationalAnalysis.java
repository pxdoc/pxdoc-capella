package org.pragmaticmodeling.pxdoc.capella.common;

import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.polarsys.capella.core.data.oa.OperationalAnalysis;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;

@SuppressWarnings("all")
public class PxOperationalAnalysis extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public PxOperationalAnalysis(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void operationalAnalysis(final ContainerElement parent, final OperationalAnalysis oa, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    createText(lObj0, oa.getName());
    _CommonServices.optionalDocumentation(parent, oa);
    List<Diagram> oebdDiagrams = this.getDiagramProvider().createDiagramsQuery(oa).searchNested().type("OEBD").execute();
    boolean _isEmpty = oebdDiagrams.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj1 = createStyleApplication(parent, false, getBindedStyle("SectionTitle"), null, null, null);
      createText(lObj1, "Operational Entities Overview");
      Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj2 = getIterator(oebdDiagrams);
      while (lObj2.hasNext()) {
        _DiagramServices.includeDiagram(parent, lObj2.next());
        
      }
    }
    HeadingN lObj3 = createHeadingN(parent, (level + 1), false);
    createText(lObj3, "Operational Entities");
  }
}
