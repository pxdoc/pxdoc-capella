package org.pragmaticmodeling.pxdoc.capella.common;

import org.eclipse.emf.ecore.EObject;
import org.pragmaticmodeling.pxdoc.plugins.html2pxdoc.DefaultHtml2PxdocExtension;

import fr.pragmaticmodeling.pxdoc.NumberingKind;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public class HtmlExtension extends DefaultHtml2PxdocExtension implements IPxDocRendererExtension {

	public HtmlExtension(IPxDocGenerator generator) {
		super(generator);
	}

	@Override
	public String adaptHref(String href) {
		return null;
	}

	@Override
	public String getHrefBookmark(String href) {
		String bookmark = href;
		if (href != null) {
			if (href.startsWith("hlink://")) {
				int lastIndex = href.lastIndexOf("/");
				if (lastIndex < 9) {
					bookmark = href.substring(8);
				} else {
					bookmark = href.substring(8, lastIndex);
				}
			}
		}
		return "b" + bookmark;
	}
	
	@Override
	public String getNumberingStyle(EObject context, NumberingKind kind) {
		if (getPxDocGenerator() != null && kind == NumberingKind.BULLET) {
			return getPxDocGenerator().getBindedStyle("BulletList");
		}
		return super.getNumberingStyle(context, kind);
	}

}
