package org.pragmaticmodeling.pxdoc.capella.common;

import org.eclipse.emf.ecore.EObject;
import org.polarsys.capella.core.data.capellacore.CapellaElement;
import org.polarsys.capella.core.data.capellacore.Namespace;
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;

public class CapellaDescriptionProvider extends DefaultDescriptionProvider {

	// private IPxDocGenerator generator;
	// private IPxDocRendererExtension htmlExtension;

	public CapellaDescriptionProvider(IPxDocGenerator generator) {
		super(generator);
	}

	@Override
	public String getDescription(Object object) {
		if (object instanceof CapellaElement) {
			return ((CapellaElement) object).getDescription();
		}
		return super.getDescription(object);
	}

	@Override
	public String getDescriptionLanguage(Object object) {
		if (object instanceof CapellaElement) {
			return "html";
		}
		return super.getDescriptionLanguage(object);
	}

	@Override
	public IPxDocRendererExtension getDescriptionLanguageExtension(Object object) {
		if (object instanceof CapellaElement) {
			if (!getLanguageExtensions().containsKey("html")) {
				getLanguageExtensions().put("html", new HtmlExtension(getGenerator()));
			}
			return getLanguageExtensions().get("html");
		}
		return super.getDescriptionLanguageExtension(object);
	}

	@Override
	public String getValidBookmark(Object object) {
		if (object instanceof EObject) {
			return "b" + getId((EObject) object);
		}
		return super.getValidBookmark(object);
	}

	// FIXME works only for eobjects contained in a resource...
	private String getId(EObject eObject) {
		if (eObject instanceof Diagram) {
			return ((Diagram)eObject).getId();
		}
		return eObject.eResource().getURIFragment(eObject);
	}

	@Override
	public boolean hasBookmark(Object object) {
		return object instanceof Namespace || object instanceof Diagram;
	}
}
