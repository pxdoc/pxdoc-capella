package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;
		
import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.LAExampleModule;

import com.google.inject.Guice;

import fr.pragmaticmodeling.common.guice.Modules2;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.AbstractPxGuiceAwarePlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxGuiceAwareUiPlugin;
		
/**
* The activator class controls the plug-in life cycle
*/
public class LAExampleActivator extends AbstractPxGuiceAwareUiPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui";
	public static final String GENERATOR_PLUGIN_ID = "org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture";
	
	private static Logger logger = Logger.getLogger(AbstractPxGuiceAwarePlugin.class);
	
	private static LAExampleActivator instance;
	
			
	/**
	 * The constructor
	 */
	public LAExampleActivator() {
		super();
		instance = this;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			setInjector(Guice.createInjector(Modules2.mixin(new LAExampleUiModule(this), new LAExampleModule())));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}
	
	protected String getPluginId() {
		return PLUGIN_ID;	
	}
	
	public String getPxDocGeneratorPluginId() {
		return GENERATOR_PLUGIN_ID;	
	}
	
	public static LAExampleActivator getInstance() {
		return instance;
	}
		
}