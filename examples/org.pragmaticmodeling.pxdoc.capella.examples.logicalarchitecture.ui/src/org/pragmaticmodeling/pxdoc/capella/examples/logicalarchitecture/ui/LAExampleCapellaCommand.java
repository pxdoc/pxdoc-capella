package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.capella.AbstractCapellaWizardCommand;

public class LAExampleCapellaCommand extends AbstractCapellaWizardCommand {
	
	public LAExampleCapellaCommand(Object selection) {
		super(selection);
	}

}

