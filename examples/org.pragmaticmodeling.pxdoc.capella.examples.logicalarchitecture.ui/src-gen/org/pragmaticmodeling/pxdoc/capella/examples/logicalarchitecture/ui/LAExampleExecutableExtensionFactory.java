package org.pragmaticmodeling.pxdoc.capella.examples.logicalarchitecture.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class LAExampleExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return LAExampleActivator.getInstance().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return LAExampleActivator.getInstance().getInjector();
	   }
	   
}
	
