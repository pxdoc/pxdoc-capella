package org.pragmaticmodeling.pxdoc.enablers.capella.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.capella.ICapellaInputContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class CapellaCommandFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private ICapellaInputContext context;
  
  public CapellaCommandFile(final ICapellaInputContext context) {
    super();
    this.context = context;
  }
  
  public ICapellaInputContext getContext() {
    return this.context;
  }
  
  public void setContext(final ICapellaInputContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.capella.AbstractCapellaWizardCommand;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append("CapellaCommand extends AbstractCapellaWizardCommand {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public ");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1, "\t");
    _builder.append("CapellaCommand(Object selection) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("super(selection);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
}
