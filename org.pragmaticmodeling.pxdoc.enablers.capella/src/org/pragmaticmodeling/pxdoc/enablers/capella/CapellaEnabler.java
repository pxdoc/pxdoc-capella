package org.pragmaticmodeling.pxdoc.enablers.capella;

import java.util.List;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class CapellaEnabler extends AbstractPlatformEnabler {

	public CapellaEnabler() {
		super();
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.capella.CapellaInput";
	}

	@Override
	public List<String> getDevelopmentTimesBundles() {
		List<String> result = super.getDevelopmentTimesBundles();
		result.add("org.pragmaticmodeling.pxdoc.enablers.capella");
		return result;
	}

	@Override
	public String getModelObject() {
		return "org.polarsys.capella.common.data.modellingcore.ModelElement";
	}

	@Override
	public List<String> getRequiredBundles(boolean withPxdocLibs) {
		List<String> result = super.getRequiredBundles(withPxdocLibs);
		result.add("org.pragmaticmodeling.pxdoc.runtime.capella");
		result.add("org.polarsys.capella.core.data.gen;visibility:=reexport");
		result.add("org.pragmaticmodeling.pxdoc.plugins.diagrams");
		result.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.diagrams.lib");
			result.add("org.pragmaticmodeling.pxdoc.capella.common");
			result.add("org.pragmaticmodeling.pxdoc.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.emf.lib");
		}
		return result;
	}

	@Override
	public String getRootTemplateDeclarations() {
		String result = "// Commons lib configuration : set a description provider able to get information about object description\n";
		result += "descriptionProvider = new CapellaDescriptionProvider(this)\n";
		result += "// Diagrams lib configuration : Capella Diagrams Provider injected in DiagramServices\n";
		result += "diagramProvider = new CapellaDiagramsProvider\n";
		return result;
	}

	@Override
	public List<String> getWithModules() {
		List<String> result = super.getWithModules();
		result.add("org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices");
		result.add("org.pragmaticmodeling.pxdoc.capella.common.CapellaDiagrams");
		result.add("org.pragmaticmodeling.pxdoc.common.lib.CommonServices");
		result.add("org.pragmaticmodeling.pxdoc.emf.lib.EmfServices");
		return result;
	}

	@Override
	public List<String> getPxdocImports(boolean withPxdocLibs) {
		List<String> result = super.getPxdocImports(withPxdocLibs);
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.capella.common.CapellaDescriptionProvider");
			result.add("org.pragmaticmodeling.pxdoc.runtime.capella.CapellaDiagramsProvider");
		}
		return result;
	}

}
