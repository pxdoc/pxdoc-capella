# Capella Enabler

This enabler provides full pxDoc capabilities to Capella.
## Quick start
###Browse the examples
Once installed, you can import the dedicated Capella generic documentation generator example.

copies d’écrans

### Initialize a new pxDoc generator
copies d’écrans manips pxGen

### Query Capella Diagrams
pxDoc provides an extension framework to manage diagram queries.
You first need to :
add a dependency to the plugin org.pragmaticmodeling.pxdoc.diagrams.lib
copie d’écran
import the DiagramsService module into your pxDoc generator
copie d’écran

Instantiate the Capella diagram provider, and setup the DiagramsService module with that instance:
example

Create your queries:
examples
Installation
The Capella enabler can be installed from the following update-site.

First make sure that pxDoc is already installed. If this is not the case :
Choose Help -> Install New Software... from the menu bar and Add....
Insert the http://www.pxdoc.fr/updates/releases URL.
Select the pxDoc Complete SDK from the category pxDoc and complete the wizard by clicking the Next button until you can click Finish.
After a quick download and a restart of Eclipse, pxDoc is ready to use.
Choose Help -> Install New Software... from the menu bar and Add....
Insert the http://www.pxdoc.fr/updates/pxdoc-capella URL.
Select the pxDoc Capella Integration from the category pxDoc Integrations and complete the wizard by clicking the Next button until you can click Finish.
After a quick download and a restart of Eclipse, the pxDoc Capella enabler is ready to use.
