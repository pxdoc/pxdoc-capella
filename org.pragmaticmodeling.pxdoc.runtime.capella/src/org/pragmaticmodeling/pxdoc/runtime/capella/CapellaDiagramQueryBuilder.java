package org.pragmaticmodeling.pxdoc.runtime.capella;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQuery;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;
import org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramQueryBuilderImpl;

public class CapellaDiagramQueryBuilder extends DiagramQueryBuilderImpl implements ICapellaDiagramQueryBuilder {

	public CapellaDiagramQueryBuilder(Object namespace, IDiagramsQueryRunner runner) {
		super(namespace, runner);
	}

	@Override
	public ICapellaDiagramQueryBuilder visibleInDoc() {
		this.getCapellaQuery().setVisibleInDoc(true);
		return this;
	}

	@Override
	public ICapellaDiagramQuery getCapellaQuery() {
		return (ICapellaDiagramQuery)getQuery();
	}
	
	@Override
	protected IDiagramQuery createQuery() {
		return new CapellaDiagramQuery();
	}

}
