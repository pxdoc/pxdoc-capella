package org.pragmaticmodeling.pxdoc.runtime.capella;

import org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramQueryImpl;

public class CapellaDiagramQuery extends DiagramQueryImpl implements ICapellaDiagramQuery {

	private boolean visibleInDoc;
	@Override
	public boolean visibleInDoc() {
		return visibleInDoc;
	}

	@Override
	public void setVisibleInDoc(boolean visibleInDoc) {
		this.visibleInDoc = visibleInDoc;
	}
	
}
