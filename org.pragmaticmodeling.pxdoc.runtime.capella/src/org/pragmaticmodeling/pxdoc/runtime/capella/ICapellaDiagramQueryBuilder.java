package org.pragmaticmodeling.pxdoc.runtime.capella;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;

public interface ICapellaDiagramQueryBuilder extends IDiagramQueryBuilder {

	//<eAnnotations xmi:type="description:DAnnotation" xmi:id="_VygmQLwdEeSJUNJMyfAosg" source="http://www.polarsys.org/capella/core/NotVisibleInDoc"/>
	ICapellaDiagramQueryBuilder visibleInDoc();

	ICapellaDiagramQuery getCapellaQuery();
}
