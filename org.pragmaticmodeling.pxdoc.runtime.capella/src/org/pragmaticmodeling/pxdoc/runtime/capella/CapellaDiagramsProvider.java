package org.pragmaticmodeling.pxdoc.runtime.capella;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;

public class CapellaDiagramsProvider implements IDiagramProvider<ICapellaDiagramQueryBuilder> {

	private IDiagramsQueryRunner runner;
	
	public CapellaDiagramsProvider() {
		super();
		this.runner = new CapellaDiagramsQueryRunner();
	}

	@Override
	public ICapellaDiagramQueryBuilder createDiagramsQuery(Object namespace) {
		return createQuery(namespace);
	}

	@Override
	public ICapellaDiagramQueryBuilder createQuery(Object namespace) {
		return new CapellaDiagramQueryBuilder(namespace, runner);
	}

}
