package org.pragmaticmodeling.pxdoc.runtime.capella;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQuery;

public interface ICapellaDiagramQuery extends IDiagramQuery {

	boolean visibleInDoc();
	void setVisibleInDoc(boolean visibleInDoc);
	
}
