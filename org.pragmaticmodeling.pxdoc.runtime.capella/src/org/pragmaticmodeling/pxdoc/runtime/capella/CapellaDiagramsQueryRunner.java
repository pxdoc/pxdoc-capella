package org.pragmaticmodeling.pxdoc.runtime.capella;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.dialect.DialectManager;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.common.tools.api.resource.ImageFileFormat;
import org.eclipse.sirius.diagram.DDiagram;
import org.eclipse.sirius.ui.business.api.dialect.DialectUIManager;
import org.eclipse.sirius.ui.business.api.dialect.ExportFormat;
import org.eclipse.sirius.ui.business.api.dialect.ExportFormat.ExportDocumentFormat;
import org.eclipse.sirius.viewpoint.DRepresentationDescriptor;
import org.eclipse.sirius.viewpoint.description.DAnnotation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.polarsys.capella.common.ef.command.AbstractReadWriteCommand;
import org.polarsys.capella.common.helpers.TransactionHelper;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.DiagramsFactory;
import org.pragmaticmodeling.pxdoc.diagrams.DiagramsPackage;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;

public class CapellaDiagramsQueryRunner implements IDiagramsQueryRunner {

	private static final String NOT_VISIBLE_IN_DOC = "http://www.polarsys.org/capella/core/NotVisibleInDoc";

	@Override
	public List<Diagram> execute(IDiagramQueryBuilder queryBuilder) {
		ICapellaDiagramQuery query = ((ICapellaDiagramQueryBuilder) queryBuilder).getCapellaQuery();
		final List<Diagram> diagrams = new ArrayList<Diagram>();
		final EObject namespace = (EObject) query.getNamespace();
		final String kind = query.getType();
		final String name = query.getName();
		final String nameStartsWith = query.getNameStartsWith();
		final boolean ignoreCase = query.ignoreCase();
		final boolean searchNested = query.searchNested();
		final boolean notEmpty = query.notEmpty();
		final boolean visibleInDoc = query.visibleInDoc();

		final Session session = SessionManager.INSTANCE.getSession(namespace);
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				TransactionHelper.getExecutionManager(namespace).execute(new AbstractReadWriteCommand() {

					@Override
					public void run() {
						try {
							if (namespace == null)
								return;

							DiagramsFactory factory = DiagramsPackage.eINSTANCE.getDiagramsFactory();
							List<DRepresentationDescriptor> diagramDescriptors = new ArrayList<DRepresentationDescriptor>();

							diagramDescriptors
									.addAll(DialectManager.INSTANCE.getRepresentationDescriptors(namespace, session));
							if (searchNested) {
								Iterator<EObject> nestedElementsIterator = namespace.eAllContents();
								while (nestedElementsIterator.hasNext()) {
									EObject nextElement = (EObject) nestedElementsIterator.next();
									diagramDescriptors.addAll(
											DialectManager.INSTANCE.getRepresentationDescriptors(nextElement, session));
								}
							}

							for (int i = 0; i < diagramDescriptors.size(); i++) {
								DRepresentationDescriptor descriptor = diagramDescriptors.get(i);
								if (!(descriptor.getRepresentation() instanceof DDiagram)) {
									continue;
								}
								DDiagram diagram = (DDiagram) descriptor.getRepresentation();
								if (matches(descriptor, name, kind, nameStartsWith, ignoreCase, notEmpty,
										visibleInDoc)) {
									String id = diagram.eResource().getURIFragment(diagram);
									File tempFile = File.createTempFile(id, ".png");
									IPath path = new Path(tempFile.getAbsolutePath());
							        ExportFormat exportFormat = new ExportFormat(ExportDocumentFormat.NONE, ImageFileFormat.PNG, ExportFormat.ScalingPolicy.NO_SCALING);
									DialectUIManager.INSTANCE.export(descriptor.getRepresentation(), session, path,
											exportFormat, new NullProgressMonitor());
									Image image = new Image(Display.getDefault(), tempFile.getAbsolutePath());
									Diagram pxdocDiagram = factory.createDiagram();
									pxdocDiagram.setName(descriptor.getRepresentation().getName());
									pxdocDiagram.setPath(tempFile.getAbsolutePath());
									pxdocDiagram.setWidth(image.getBounds().width);
									pxdocDiagram.setHeight(image.getBounds().height);
									pxdocDiagram.setDocumentation(getDiagramDescription(diagram));
									pxdocDiagram.setId(id);
									pxdocDiagram.setDiagramObject(diagram);
									diagrams.add(pxdocDiagram);
									image.dispose();
									tempFile.deleteOnExit();
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});

		return diagrams;
	}

	private boolean matches(DRepresentationDescriptor descriptor, String name, String kind, String nameStartsWith,
			boolean ignoreCase, boolean notEmpty, boolean visibleInDoc) {
		DDiagram diagram = (DDiagram) descriptor.getRepresentation();
		String currentKind = descriptor.getDescription().getTitleExpression();
		currentKind = currentKind.substring(currentKind.indexOf("[") + 1, currentKind.indexOf("]"));
		String currentName = diagram.getName();
		if (name != null) {
			if (ignoreCase && !name.equalsIgnoreCase(currentName))
				return false;
			if (!ignoreCase && !name.equals(currentName))
				return false;
		}
		if (kind != null && !kind.equalsIgnoreCase(currentKind))
			return false;
		if (nameStartsWith != null) {
			if (ignoreCase && !currentName.toLowerCase().startsWith(nameStartsWith.toLowerCase()))
				return false;
			if (!ignoreCase && !currentName.startsWith(nameStartsWith))
				return false;
		}
		if (notEmpty) {
			if (isEmpty(diagram))
				return false;
		}
		if (visibleInDoc) {
			for (DAnnotation a : diagram.getEAnnotations()) {
				if (NOT_VISIBLE_IN_DOC.equals(a.getSource())) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean isEmpty(DDiagram diagram) {
		return diagram.getDiagramElements().isEmpty();
	}

	public static String getDiagramDescription(DDiagram diagram) {
		return diagram.getDocumentation();
	}
}
